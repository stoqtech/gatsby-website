# Stoq Website

Using Gatsby and the <a href="https://github.com/arshad/gatsby-themes/tree/master/themes/gatsby-theme-flex">gatsby-theme-flex</a> theme.

<a href="https://stoq.netlify.com/"><img src="https://img.shields.io/badge/demo-netlify-success" alt="Demo"></a>

## Development

### Setup

```shell
npm i
```

### Start the development environment

```shell
npm run dev
```

Then access `http://localhost:8000/` in your browser.

### Hack Hack Hack

Check out the docs for [gatsby-theme-flex](https://flex.arshad.io/docs).

### Testing

TBD

## Deploy

First build the project

```shell
npm run build
```

After the build rename the public folder

```shell
mkdir www
mv public www/dist
```

Then create the artifacts file

```shell
zip -r artifacts.zip www
```

Then run the deploy script, and follow the instructions

```shell
python3 deploy.py
```
