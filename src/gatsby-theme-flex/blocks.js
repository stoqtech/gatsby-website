// Components available in MDX files.

// External.
export { Grid, Flex } from "@theme-ui/components"
export { default as MediaQuery } from 'react-responsive'

// Components.
export { default as Button } from "gatsby-theme-flex/src/components/button"
export { default as Card } from "gatsby-theme-flex/src/components/card"
export { default as Faq } from "gatsby-theme-flex/src/components/faq"
export { default as Lead } from "gatsby-theme-flex/src/components/lead"
export { default as Link } from "gatsby-theme-flex/src/components/link"
export { default as Logo } from "gatsby-theme-flex/src/components/logo"
export { default as Testimonial } from "gatsby-theme-flex/src/components/testimonial"

// Blocks.
export { default as Div } from "gatsby-theme-flex/src/blocks/div"
export { default as Faqs } from "gatsby-theme-flex/src/blocks/faqs"
export { default as Feature } from "gatsby-theme-flex/src/blocks/feature"
export { default as Logos } from "gatsby-theme-flex/src/blocks/logos"
export { default as PageHeader } from "gatsby-theme-flex/src/blocks/page-header"
export { default as Section } from "gatsby-theme-flex/src/blocks/section"

// Custom Blocks.
export { default as Desktop } from "../components/desktop"
export { default as Mobile } from "../components/mobile"
export { default as CardCase } from "../components/card-case"
export { default as ContactForm } from "../components/contact-form"
export { default as Image } from "../components/image"
export { default as Helmet } from "../components/helmet"
export { default as Cta } from "../blocks/cta"
export { default as Hero } from "./hero"

// SVG images
export { default as ConfirmedSVG } from "../components/confirmed-svg"
