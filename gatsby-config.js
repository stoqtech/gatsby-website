require(`dotenv`).config()

module.exports = {
  siteMetadata: {
    title: `Stoq Tecnologia`,
    description: `Stoq Tecnologia`,
    siteUrl: process.env.SITE_URL,
    logo: `logo.png`,
    copyright: `© 2020 Stoq Tecnologia. All rights reserved.`,
    headerLinks: [
      {
        title: `Principal`,
        path: `/`,
      },
      {
        title: `Sobre`,
        path: `/about`,
      },
    ]
  },
  plugins: [
    `gatsby-theme-flex`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-165030656-1",
        anonymize: true,
        respectDNT: true,
      },
    }
  ]
}
