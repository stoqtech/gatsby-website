import os
from datetime import datetime
from urllib.parse import quote

import click
import requests
from fabric import Connection


OLD_LOCATIONS = { 'www': '/tmp/www/dist'}
PROJECT_NAMES = { 'www': 'stoq.com.br'}


def _get_new_location(project_name, stage):
    now = datetime.now()
    new_folder_name = "{}-{}".format(stage, now.strftime("%Y-%m-%d-%H-%M-%S"))
    new_location = '/var/www/{}/builds/{}'.format(project_name, new_folder_name)

    return new_location


def _transfer_artifact(zipfile_name, hostname, stage, passphrase):
    home = os.environ['HOME']
    location = '/tmp'
    connect_kwargs = {"key_filename": "{}/.ssh/fab-machines.pem".format(home),
                      "passphrase": passphrase}
    connection = Connection(hostname, user='root', connect_kwargs=connect_kwargs)
    with connection as c:
        # Send zipfile to remote
        c.put(zipfile_name, location)

        # Decompress zipfile
        c.run("unzip {location}/{zipfile_name} -d '{location}'".format(zipfile_name=zipfile_name,
                                                                       location=location))

        # Move decompressed folder
        project_name = PROJECT_NAMES["www"]
        old_location = OLD_LOCATIONS["www"]
        new_location = _get_new_location(project_name, stage)
        c.run("mv {} {}".format(old_location, new_location))

        # Update stoq-link
        c.run("ln -nsf {} /var/www/{}/builds/{}-current".format(new_location, project_name, stage))


@click.command()
@click.option('--hostname',
              prompt='Hostname',
              default='hydra.async.com.br',
              show_default=True)
@click.option('--stage',
              prompt='Subdomain',
              default='preview',
              show_default=True,
              type=click.Choice(['homolog', 'preview', 'production'], case_sensitive=False))
@click.option('--passphrase',
              prompt='fab-machines.pem password',
              hide_input=True)
def main(hostname, stage, passphrase):
    zipfile_name = 'artifacts.zip'

    # Transfer the artifact to the remote machine and set it up
    _transfer_artifact(zipfile_name, hostname, stage, passphrase)


if __name__ == '__main__':
    main()
